;; RESTful API v1 definition of pylon
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-restful-api v1) ; DO NOT REMOVE THIS LINE!!!

;; helper method for rc to json
(define rc-body->json->scm (lambda (rc)
                             (let* ((body (rc-body rc))
                                    (body-str (if (bytevector? body) (utf8->string body) #f)))
                               (json-string->scm body-str))))

;; method for getting an actor TODO: impl
(define get-actor (lambda (actorId)
                    '(("@context" . "https://www.w3.org/ns/activitystreams")
                      ("type" . "Person")
                      ("id" . "http://localhost:3000/v1/actor?id=1")
                      ("followers" . "http://localhost:3000/v1/followers/1/1")
                      ("following" . "http://localhost:3000/v1/following/1/1")
                      ("liked" . "http://localhost:3000/v1/liked/1/1")
                      ("inbox" . "http://localhost:3000/v1/inbox/1/1")
                      ("outbox" . "http://localhost:3000/v1/outbox/1/1")
                      ("preferredUsername" . "pylon-test")
                      ("name" . "Pylon Test Account")
                      ("summary" . "A pylon test account"))))

;; method for getting followers TODO: impl
(define get-followers (lambda (actorId pageNum)
                     '(("@context" . "https://www.w3.org/ns/activitystreams")
                       ("summary" . "Followers of the pylon test user")
                       ("type" . "OrderedCollectionPage")
                       ("id" . "http://localhost:3000/v1/followers/1/1")
                       ("partOf" . "http://localhost:3000/v1/followers/1/1")
                       ("orderedItems" . #(
                                           (get-actor 1)
                                           (get-actor 2)))
                       ("next" . "http://localhost:3000/v1/followers/1/2"))))

;; method for getting following TODO: impl
(define get-following (lambda (actorId pageNum)
                        '(("@context" . "https://www.w3.org/ns/activitystreams")
                          ("summary" . "Following of the pylon test user")
                          ("type" . "OrderedCollectionPage")
                          ("id" . "http://localhost:3000/v1/following/1/1")
                          ("partOf" . "http://localhost:3000/v1/following/1/1")
                          ("orderedItems" . #(
                                              (get-actor 1)
                                              (get-actor 2)))
                          ("next" . "http://localhost:3000/v1/following/1/2"))))

;; method for getting liked activities TODO: impl
(define get-liked (lambda (actorId pageNum)
                     '(("@context" . "https://www.w3.org/ns/activitystreams")
                       ("summary" . "Likes of the pylon test user")
                       ("type" . "OrderedCollectionPage")
                       ("id" . "http://localhost:3000/v1/likes/1/1")
                       ("partOf" . "http://localhost:3000/v1/likes/1/1")
                       ("orderedItems" . #(
                                           (("type" . "Note")
                                            ("name" . "Note 1"))
                                           (("type" . "Note")
                                            ("name" . "Note 2"))))
                       ("next" . "http://localhost:3000/v1/likes/1/2"))))

;; method for getting an outbox TODO: impl
(define get-outbox (lambda (actorId pageNum)
                     '(("@context" . "https://www.w3.org/ns/activitystreams")
                       ("summary" . "Outbox of the pylon test user")
                       ("type" . "OrderedCollectionPage")
                       ("id" . "http://localhost:3000/v1/outbox/1/1")
                       ("partOf" . "http://localhost:3000/v1/outbox/1/1")
                       ("orderedItems" . #(
                                          (("type" . "Note")
                                            ("name" . "Note 1"))
                                          (("type" . "Note")
                                            ("name" . "Note 2"))))
                       ("next" . "http://localhost:3000/v1/outbox/1/2"))))

;; method for getting an inbox TODO: impl
(define get-inbox (lambda (actorId pageNum)
                     '(("@context" . "http://www.w3.org/ns/activitystreams")
                       ("summary" . "Inbox of the pylon test user")
                       ("type" . "OrderedCollectionPage")
                       ("id" . "http://localhost:3000/v1/inbox/1/1")
                       ("partOf" . "http://localhost:3000/v1/inbox/1/1")
                       ("orderedItems" . #((("type" . "Note") ("name" . "Note 1")) (("type" . "Note")("name" . "Note 2"))))
                       ("next" . "http://localhost:3000/v1/inbox/1/2"))))

(api-define activity/list
            (options #:mime 'json)
            (lambda (rc)
              (let ((resp '(("@context" . "https://www.w3.org/ns/activitystreams")
                            ("summary" . "a sample")
                            ("type" . "Note")
                            ("content" . "Hello, world!"))))
                (:mime rc resp))))

(api-define activity/create
            (method post)
            (options #:mime 'json)
            (lambda (rc)
              (let ((resp '(("@context" . "https://www.w3.org/ns/activitystreams")
                           ("summary" . "new sample")
                           ("type" . "Note")
                           ("content" . "Sample created!"))))
                (:mime rc resp))))

;; GET actor?id=actorId
;; gets an actor/person
(api-define actor/:id
            (method get)
            (options #:mime 'json)
            (lambda (rc)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr)))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (get-actor actorId))))))


;; POST outbox/:id
;; submits a post to an actor's outbox
(api-define outbox/:id
            (method post)
            (options #:mime 'json #:from-post 'json)
            (lambda (rc)
              (display "outbox id: ")(display (params rc "id")) (newline)
              (display "RC body: ")(display (utf8->string(rc-body rc))) (newline)
              (display "JSON body: ")(display (rc-body->json->scm rc)) (newline)
              (display "------------------") (newline)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr)))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (rc-body->json->scm rc))))))

;; POST inbox/:id
;; submits a post to an actor's inbox
(api-define inbox/:id
            (method post)
            (options #:mime 'json #:from-post 'json)
            (lambda (rc)
              (display "inbox id: ")(display (params rc "id")) (newline)
              (display "RC body: ")(display (utf8->string(rc-body rc))) (newline)
              (display "JSON body: ")(display (rc-body->json->scm rc)) (newline)
              (display "------------------") (newline)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr)))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (rc-body->json->scm rc))))))

;; GET inbox/:id
;; gets an inbox's :page
(api-define inbox/:id
            (method get)
            (options #:mime 'json)
            (lambda (rc)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr))
                     (pageNum 1))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (get-inbox actorId pageNum))))))

;; GET inbox/:id/:page
;; gets an inbox's :page
(api-define inbox/:id/:page
            (method get)
            (options #:mime 'json)
            (lambda (rc)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr))
                     (pageStr (params rc "page"))
                     (pageNum (if (nil? pageStr) 1 (begin (let ((num (string->number pageStr))) (if (nil? num) 1 num))))))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (get-inbox actorId pageNum))))))

;; GET outbox/:id
;; gets an outbox
(api-define outbox/:id
            (method get)
            (options #:mime 'json)
            (lambda (rc)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr))
                     (pageNum 1))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (get-outbox actorId pageNum))))))

;; GET outbox/:id/:page
;; gets an outbox
(api-define outbox/:id/:page
            (method get)
            (options #:mime 'json)
            (lambda (rc)
              (let* ((actorStr (params rc "id"))
                     (actorId (string->number actorStr))
                     (pageStr (params rc "page"))
                     (pageNum (if (nil? pageStr) 1 (begin (let ((num (string->number pageStr))) (if (nil? num) 1 num))))))
                (if (nil? actorId)
                    (http-status 404
                                 (lambda () "Not found!"))
                    (:mime rc (get-outbox actorId pageNum))))))

